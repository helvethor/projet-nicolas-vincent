import random

alphabet = "abcdefghijklmnopqrstuvwxyz"

def generate_random_string(min, max, charset = alphabet):
    
    length = random.randint(min, max)
    string = ""
    
    for i in range(length):
        string += random.choice(charset)
    
    return string

def generate_url():

    subdomain = generate_random_string(5, 15, alphabet)
    
    domain = random.choice(["en", "fr", "ch", "com", "net", "org", "be", "de", "it"])
    
    nb_path = random.randint(0, 3)
    path = "/"
    for i in range(nb_path):
        add_to_path = generate_random_string(3,25, alphabet) + "/"
        path += add_to_path
        
    doc = generate_random_string(3,7, alphabet) + "." + random.choice(["html", "docx", "css", "pdf", "jpg", "htm"])
    
    url = "http://" + subdomain + "." + domain + path + doc

    return url

def generate_random_index(size = random.randint(1,30)):

    index = []
    for i in range(size):
        urls = []
        for i in range(random.randint(1,10)):
            urls += [generate_url()]
        index += [[generate_random_string(1,15), urls]]

    return index
        
