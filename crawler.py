# auteur : Cédric Donner et la classe d'OCI

# Dans ce fichier, nous placerons les fonctions qui concernent le robot
# d'indexation

#les accents dans les commentaires produiront des erreurs lors de l'execution du fichier avec le bouton 'run'. Signe: Simon et Emerald ;-)

import urllib.request
import index_bench
import indexer


# Fonction définie dans les vidéos 
# (corrigé détaillé sur http://www.donner-online.ch/oci/projet-1/corrige/partie-01/10-etape-01.html#solution-video-pas-a-pas-linux-ubuntu)



def get_page(url):
    try:
        fd = urllib.request.urlopen(url)
        html = fd.read()

        # il faut convertir les données en str car elles sont lues en bytes
        # depuis urllib
        # http://stackoverflow.com/a/542080
        
        return str(html)
        
    except:
        return ''



def get_next_target(page, start=0):
   
    '''
    
    Retourne une tuple (url, end) où url  est 
    l'URL (target) du premier lien rencontré dans le 
    code HTML ``page`` à partir de la position
    ``start`` et ``end`` indique la position de la
    fin de l'URL dans ``page``.
    
    S'il n'y a plus de lien après la position start, 
    la fonction retourne le tuple (None, -1).

    '''
    
    url = None
    pattern = '<a href="'
    start_tag = page.find(pattern, start)
    
    if start_tag == -1:
        return (None, -1)
    
    start_url = start_tag + len(pattern)
    end_url = page.find('"', start_url)
    
    url = page[start_url:end_url]
	
    return (url, end_url+1)



def test_get_next_target():
    
    html = '''<html>\n    <body>\n        <a href="url1">Lien1</a>\n        un peu de texte\n        <a href="url2">Lien2</a>\n    </body>\n</html>\n'''
    print(get_next_target(html, start=0)) # ==> ('url1', 40)
    print(get_next_target(html, start=39)) # ==> ('url2', 97)
    print(get_next_target(html, start=97)) # ==> (None, -1)
    print(get_next_target(''))



def get_all_links(url_source, index = []):
    
    '''
    
    Récupère tous les liens trouvés sur la page d'une url donnée et raffine le 
    résultat pour n'obtenir que des liens absolus.
    
    Pour chaque mot trouvé sur la page l'url source est ajoutée à l'index.
    
    '''
    
    page = get_page(url_source)
    links = []
    url, end_url = get_next_target(page, 0)
	
	
    while end_url != -1:
        
        if url.find("#") == -1 and url != "": # Filtrage des ancrages et des url vides
        
            if url[0] == "/": # Réparation des liens relatifs à la racine du site
                root = url_source[0 : url_source.find("/", 9)]
                url = root + url
                
            elif url[0:4] != "http": # Réparation des liens relatifs à la page actuelle
                folder = url_source[0 : url_source.rfind("/") + 1]
                url = folder + url

            links += [url]
        url, end_url = get_next_target(page, end_url)
    
    keywords = page.lower().split(" ")
    for keyword in keywords:
        indexer.add_to_index(index, keyword, url_source)

    return links, index



def test_get_all_links():
    urls = ["http://www.donner-online.ch/oci/projet-1/test-crawl-web/index.html","http://www.donner-online.ch/oci/projet-1/test-crawl-web/page1.html","http://www.donner-online.ch/oci/projet-1/test-crawl-web/page2.html","http://www.donner-online.ch/oci/projet-1/test-crawl-web/page4.html"]

    for url in urls:
        print(get_all_links(url)[0])
        


def crawl_web(seed, verbose = False):
    
    '''
    
    Navigue de page en page à partir d'une url seed.
    La recherche est faites selon le modèle "first in first out", "fifo".
    
    '''
    
    to_crawl, index = get_all_links(seed, [])
    crawled = [seed]
    
    for link in to_crawl:
        result = get_all_links(link, index) # Connaissez vous une méthode plus élégante, puisque index est assigné mais to_crawl est incrémenté?.
        to_crawl += result[0]
        index = result[1]
        crawled += [link]
        
    if verbose:

        print("\n\n\n--------------<<< INDEX >>>--------------\n\n\n")
        for entry in index:
            print(entry[0] + "\n" + str(entry[1]) + "\n\n", end="")

        print("\n\n\n--------------<<< URLS >>>--------------\n\n\n")
        for url in crawled:
            print(url)
	


def test_crawl_web():

    crawl_web("http://www.donner-online.ch/oci/projet-1/test-crawl-web/test/index.html", True)
    

    
def lookup(index, keyword):

    '''

    Indique toutes les urls se trouvant dans index contenant le keyword désiré.

    '''
    
    for entry in index:
        if entry[0] == keyword:
            return entry[1]
    return []

    

def test_lookup():
    index = [["dieu",["url1","url2"]],["cieux",["url3","url1"]],["commencement",["url3","url4","url1"]],["terre",["url5"]],["taynaybre",["url2"]],["poule",["url123"]]]
    keywords = ["commencement", "dieu", "cieux", "terre", "ténèbres"]
    
    for keyword in keywords:
        print(lookup(index, keyword))

    

def add_to_index(index, keyword, url):

    '''

    Associe une url à un keyword de index. Si le keyword n'est pas répértorié,
    une nouvelle entrée est crée
    
    '''
    
    for i in range(len(index)):
        if keyword == index[i][0]:
            
            for url_test in index[i][1]: #nous ne créons pas de doublons
                if url_test == url:
                    return index
                
            index[i][1] += [url]
            return index
        
    index += [[keyword,[url]]]
    return index


    
def test_add_to_index():
    
    index = []
    index = add_to_index(index, "consigne", "http://www.donner-online.ch/oci/projet-1/test-crawl-web/index.html")
    index = add_to_index(index, "récupération", "http://www.donner-online.ch/oci/projet-1/test-crawl-web/index.html")
    index = add_to_index(index, "consigne", "second lien")

    print(index)



if __name__ == '__main__':
    None
