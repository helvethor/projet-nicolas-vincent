


def get_kw(entry):
    return entry[0]



def get_urls(entry):
    return entry[1]



def add_entry_to_index(index, keyword, url):
    index.append([keyword, [url]])



def add_url_to_entry(entry, url):
    entry[1].append(url)



def get_kw_position(index, keyword):
    '''

    Retourne le numéro de l'entrée contenant le mot-clé spécifié par
    ``keyword`` dans l'index.

    Si aucune entrée de l'index ne correspond au mot-clé spécifié, la fonction
    retourne -1

    '''
    position = -1

    i = 0
    while i < len(index):
        if get_kw(index[i]) == keyword:
            position = i
            break            
        i += 1

    return position



def lookup(index, keyword):
    '''
    
    La fonction lookup permet de rechercher dans l'index toutes les urls qui
    sont associées au mot clé indiqué par ``keyword``. 

    Valeur de retour : list d'urls

    Si le mot ``keyword``
    ne figure pas encore dans l'index, la fonction retourne une liste vide.

    '''

    urls = []

    for entry in index:
        if get_kw(entry) == keyword:
            urls = get_urls(entry)
            break

    return urls

    # retourner la liste d'urls
    return urls



def add_to_index(index, keyword, url):

    # il faut s'assurer que le mot-clé n'existe pas déjà dans l'index
    position = get_kw_position(index, keyword) 
    if position == -1:
        # le mot-clé ``keyword``ne se trouve pas encore dans l'index. Il faut
        # donc rajouter une entrée dans l'index
        add_entry_to_index(index, keyword, url)

    else:
        # le mot-clé ``keyword``se trouve déjà dans l'index. Il faut donc
        # rajouter l'url à cette entrée
        add_url_to_entry(index[position], url)
        
        
        